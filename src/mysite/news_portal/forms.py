from django import forms
from news_portal import models




class NewsForm(forms.ModelForm):

    class Meta:
        model = models.News
        fields = ('subject', 'news','news_tag')

        widgets = {
            'subject': forms.TextInput(attrs={'class': 'form-control textinputclass',
                                              "placeholder":"Your News Subject",}),
            'news': forms.Textarea(attrs={'class': 'form-control editable medium-editor-textarea postcontent',
                                          }),
            # 'news_tag': forms.
        }


class CommentForm(forms.ModelForm):

    class Meta:
        model = models.Comment
        fields = ('name', 'text')

        widgets = {
            'name':forms.TextInput(attrs={
                'class':'form-control',
            }),
            'text':forms.Textarea(attrs={
                'class':'form-control',
            }),
        }
