from django.shortcuts import render, redirect, get_object_or_404
from news_portal import models
from news_portal import forms
from django.views import generic
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin



class NewsListView(generic.ListView):
    model = models.News

    def get_queryset(self):
        return models.News.objects.filter(
            published_date__lte=timezone.now()).order_by("-published_date")



class NewsDetailView(generic.DetailView):
    pk='id'
    model = models.News


class NewsCreateView(LoginRequiredMixin, generic.CreateView):
    login_url = '/login/'
    redirect_field_name = 'news_portal/news_detail.html'

    form_class = forms.NewsForm
    model = models.News



class NewsUpdateView(LoginRequiredMixin, generic.UpdateView):
    login_url = "/login/"
    redirect_field_name = "news_portal/news_detail.html"

    form_class = forms.NewsForm
    model = models.News




class NewsDeleteView(LoginRequiredMixin,generic.DeleteView):
    model = models.News
    success_url = reverse_lazy("news_portal:news_list")



class NewsDrafListView(LoginRequiredMixin, generic.ListView):
    login_url = "/login/"
    redirect_field_name = "news_portal/news_list.html"
    model = models.News


    def get_queryset(self):
        return models.News.objects.filter(
            published_date__isnull=True).order_by("created_date")
#########################################

@login_required
def add_comment_to_news(request, pk):
    news = get_object_or_404(models.News, pk=pk)

    if request.method == "POST":
        comment_form = forms.CommentForm(request.POST)

        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.news = news
            comment.save()
            return redirect("news_portal:news_detail", pk=news.pk)

    else:
        comment_form = forms.CommentForm

        content_dic = {
            'form':comment_form,
        }
    return render(request, 'news_portal/comment_form.html', content_dic)
