from django import template


register = template.Library()


@register.filter(name="all_uppercase")
def all_uppercase(value):
    return value.upper()
