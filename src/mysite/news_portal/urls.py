from django.urls import path
from news_portal import views



app_name = 'news_portal'
urlpatterns = [
    path('', views.NewsListView.as_view(), name='news_list'),
    path('news/<int:pk>/', views.NewsDetailView.as_view(), name='news_detail'),
    path('news/create/', views.NewsCreateView.as_view(), name='news_create'),
    path('news/<int:pk>/update', views.NewsUpdateView.as_view(), name='news_update'),

    path('news/<int:pk>/comment', views.add_comment_to_news, name='news_comment'),
]
