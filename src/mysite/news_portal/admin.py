from django.contrib import admin
from news_portal import models
# Register your models here.


admin.site.register(models.News)
admin.site.register(models.NewsImage)
admin.site.register(models.Comment)
