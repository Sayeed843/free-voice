# Generated by Django 3.0.8 on 2020-07-11 14:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('news_portal', '0008_auto_20200709_1358'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='news',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comment', to='news_portal.News'),
        ),
    ]
