from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.

class News(models.Model):
    NEWS_TAGS = (
        ('ENTERTAINMENT', 'ENTERTAINMENT'),
        ('BUSNIESS', 'BUSNIESS'),
        ('TRAVEL', 'TRAVEL'),
        ('INTERNATIONAL', 'INTERNATIONAL'),
        ('SPORTS', 'SPORTS'),
        ('LIFE STYLE', 'LIFE STYLE'),
        ('VIDEO', 'VIDEO'),
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="newss")
    subject = models.CharField(max_length=450, unique=True)
    news = models.TextField()
    news_tag = models.CharField(max_length=13, choices=NEWS_TAGS)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish_the_news(self):
        self.published_date = timezone.now()
        self.save()


    def get_absolute_url(self):
        return reverse("news_portal:news_detail", kwargs={"pk":self.pk})

    def __str__(self):
        return self.subject


######### NewsImage Upload Method ########

def news_image_upload(instance, filename):
    datetime = timezone.now()
    print(datetime)
    datetime = str(datetime).split(" ")
    date = datetime[0]
    time = datetime[1]
    time = time.split(".")
    time = time[0]
    return "news/"+ str(date) +"/"+ str(time) +"/"+ str(filename)

######### NewsImage Upload Method ########


####### Start NewsImage Class #######

class NewsImage(models.Model):
    news = models.ForeignKey(News, on_delete=models.CASCADE, related_name="newsimages")
    image = models.ImageField(upload_to=news_image_upload, verbose_name="News Image")


    def __str__(self):
        return self.news.subject

####### END NewsImage Class #######



####### START COMMENT CLASS ########

class Comment(models.Model):
    news = models.ForeignKey(News, on_delete=models.CASCADE, related_name="comments")
    name = models.CharField(max_length=264)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_date = models.BooleanField(default=False)


    def approved_comment(self):
        self.approved_date=True
        self.save()

    def __str__(self):
        return self.name


####### END COMMENT CLASS ########
