from django.apps import AppConfig


class NewsProtalAppConfig(AppConfig):
    name = 'news_portal'
